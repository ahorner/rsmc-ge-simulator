import java.util.Collection;
import java.util.HashMap;

public class ItemCollection extends HashMap<String, Item> {

    int num_frames

    Item create_item(String item_name) {
        float starting_price = Main.prompt("Item doesn't exist. Starting price") as float
        Item item = new Item(item_name, num_frames, starting_price)
        this.put(item_name, item)
        println "Created new item '${item_name}' with starting price: ${starting_price}"
        return item
    }

    Item get_item(String item_name) {
        Item item = this.get(item_name);
        if (item == null) {
//            item = create_item(item_name)
            println "Item ${item_name} doesn't exist"
        }
        return item;
    }

    void delete_item(String item_name) {
        if (!this.containsKey(item_name)) {
            println "Can't delete ${item_name}, it doesn't exist"
        } else {
            this.remove(item_name)
        }
    }

    float get_current_price(String item_name) {
        Item item = get_item(item_name)
        return item.current_price
    }

    void buy(String item_name) {
        Item item = get_item(item_name)
        int volume = Main.prompt("Buy volume") as int
        float price = Main.prompt("Buy price") as float
        item.buy_transaction(volume, price)
    }

    void sell(String item_name) {
        Item item = get_item(item_name)
        int volume = Main.prompt("Sell volume") as int
        float price = Main.prompt("Sell price") as float
        item.sell_transaction(volume, price)
    }

    void update(String item_name) {
        Item item = get_item(item_name)
        item.update()
    }

    void update_all_items() {
        for (Item item in this.values()) {
            item.update();
        }
    }

    String toString() {
        String output = ""
        for (item_key in this) {
            Item item = item_key.value
            output += "${item.item_name} [${item.current_price}], "
        }
        if (output.length() == 0) return output
        return output.subSequence(0, output.length()-2)
    }

}
