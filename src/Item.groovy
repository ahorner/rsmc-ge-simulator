class Item {

    String            item_name
    float             starting_price
    int               num_frames
    List<VolumeValue> price_history = new ArrayList<VolumeValue>()
    List<VolumeValue> frame_data = new ArrayList<VolumeValue>()
    float             current_price

    Item(String item_name, int num_frames, float starting_price) {
        println "New item"
        this.item_name = item_name
        this.num_frames = num_frames
        this.starting_price = starting_price
        this.current_price = starting_price
    }

    private float get_current_price() {
//        if (price_history.size() == 0) return starting_price

        //Do math here based on past frames
        int min_num_frames = (num_frames > price_history.size()) ? price_history.size() : num_frames

        List<VolumeValue> most_recent_frames = price_history[-min_num_frames..-1]
        int total_volume = 0
        float total_value = 0.0
        for (VolumeValue vv in most_recent_frames) {
            total_value += vv.value
            total_volume += vv.volume
        }
        float avg = Math.round(total_value / total_volume * 100) / 100
        return avg
    }

    void buy_transaction(int volume, float value) {
        VolumeValue transaction = new VolumeValue(volume: volume, value: value)
        frame_data.add(transaction)
    }

    void sell_transaction(int volume, float value) {
        VolumeValue transaction = new VolumeValue(volume: -volume, value: value)
        frame_data.add(transaction)
    }

    void update() {
        //Advances the frame
//        int net_volume = 0; //Number traded with selling subtracting (may be negative)
        float volume_price = 0; //Total money traded hands (abs(volume) * value)
//        float net_value = 0; //Net money (value * volume)
        float total_volume = 0; //Total volume traded hands (abs(volume))
        for (VolumeValue vv in frame_data) {
//            net_volume += vv.volume
            volume_price += (vv.value * Math.abs(vv.volume))
//            net_value += (vv.value * vv.volume)
            total_volume += Math.abs(vv.volume)
        }
//        float average = volume_price / net_volume
//        float real_average = net_value / 12

        VolumeValue historical_vv = new VolumeValue(volume: total_volume, value: volume_price)
        price_history.add(historical_vv)

        frame_data.clear()
        float old_price = this.current_price
        this.current_price = this._current_price
        println "${item_name} updated from ${old_price} to ${this.current_price}"
    }

}
