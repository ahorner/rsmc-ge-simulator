println "  _____   _____ __  __  _____ \n" +
        " |  __ \\ / ____|  \\/  |/ ____|\n" +
        " | |__) | (___ | \\  / | |     \n" +
        " |  _  / \\___ \\| |\\/| | |     \n" +
        " | | \\ \\ ____) | |  | | |____ \n" +
        " |_|  \\_\\_____/|_|  |_|\\_____|\n\n"


println "Welcome to our GE Simulator!"
frame_size = prompt("Frame size in hours", "4") as int
num_frames = prompt("Number of frames to take into account", "6") as int

int frames_per_24hr = Math.round(24.0 / frame_size * 100.0) / 100.0
println frames_per_24hr + " frames per 24hr"
//println "${frame_size} hours per frame and ${num_frames} frames"
println "One 'set' of frames lasts ${num_frames * frame_size} hours"

items = new ItemCollection(num_frames: num_frames)  //empty map syntax
input = ""
while (input != "exit" && input != "quit") {
    input = prompt("Choose from the following: new, buy, sell, update, updateall, list, delete, exit, quit")
    if (input != "list" && input != "updateall") item_name = prompt("Enter an item name")

    switch (input) {
        case "new":
            items.create_item(item_name)
            break;
        case "buy":
            items.buy(item_name)
            break;
        case "sell":
            items.sell(item_name)
            break;
        case "update":
            items.update(item_name)
            break;
        case "updateall":
            items.update_all_items()
            break;
        case "list":
            println items.toString()
            break;
        case "delete":
            items.delete_item(item_name)
            break;
        default:
            //What to do here, if anything?
            break;
    }
}






def exit() {
    println "Exit"
    System.exit();
}

static String prompt(String message, String defaultValue = null) {
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    String inputPrompt = "[Input]"
    if (defaultValue) {
        print "${inputPrompt} ${message} (default: ${defaultValue}): "
        return bufferedReader.readLine() ?: defaultValue
    }
    print "${inputPrompt} ${message}: "
    return bufferedReader.readLine() ?: defaultValue
}